// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        l1: cc.Node, //左侧背景1
		l2: cc.Node, //左侧背景2
		r1: cc.Node, //右侧背景1
		r2: cc.Node, //右侧背景2
		speed: 50, //背景移动速度
		player: cc.Node, //机器人节点
		jump: true, //是否可以跳
		box: cc.Prefab,  //石头预制体
		boxList: cc.Node, //石头集合节点
		boxCount:0, //石头个数
		boxWidth:121, //石头的宽
        boxHeight:117, //石头的高
		preBoxX: 0, //上一个石头的X坐标
		preBoxY: 0, //上一个石头的Y坐标
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
		this.node.on(cc.Node.EventType.TOUCH_START,this.onTouchStart,this)
		// 获取重置触发坐标点
		this.triggerLY = -this.l1.height;
		this.triggerRY = -this.r1.height;
		for(var i=0;i<12;i++){
			this.newBox();
		}
	},
	//生成石头
    newBox () {
		// 通过预制体复制出一个石头节点
        var newBox = cc.instantiate(this.box);
		// 把石头节点添加在石头集合节点
        this.boxList.addChild(newBox);
		// 设置石头的坐标
		var x = this.player.x+0;
		var y = this.player.y-120;
		var position = cc.Vec2(x,y);
		
		this.boxCount++;
		var randD = Math.random();
		if(this.boxCount >= 2){
			// 后面随机生成的石头
			if(randD <= 0.5){
				// 在左侧生成
				x = this.preBoxX - this.boxWidth/2;
			}else{
				// 在右侧生成
				x = this.preBoxX + this.boxWidth/2;
				
				// 第二个石头
				if(this.boxCount == 2){
					this.player.scaleX = -1;
				}
			}			
			y = this.preBoxY + 100;
			var position = cc.Vec2(x,y);
		}
		
		// 记住上一个石头的坐标
		this.preBoxX = x;
		this.preBoxY = y;
		
		newBox.setPosition(position);
		var goAction = cc.moveTo(0.1,position);
        newBox.runAction(goAction);
    },
	onTouchStart (event) {
		console.log('点击屏幕');
		if(!this.jump){
			return;
		}
		// 点击后就不让点击，直到动作执行完成
		this.jump = false;
		
		var touchLoc = event.getLocation();
        if (touchLoc.x >= cc.winSize.width/2) {
            // 向右
			this.playerMoveRight();
        } else {
            // 向左
			this.playerMoveLeft();
        }	
			
		// 生成一个石头
		this.newBox();
		this.bgMove();
	},
	playerMoveLeft () {
		console.log('向左跳跃');
		// 用0.1秒的时间，向上移动100px
		var up = cc.moveTo(0.1,cc.Vec2(this.player.x,this.player.y+100));
		// 用0.1秒的时间, 向下移动100px
		var down = cc.moveTo(0.1,cc.Vec2(this.player.x,this.player.y));
		// 执行回调
		var callback = cc.callFunc(this.jumpIsOver, this); 
		// 先向上移动，再向下移动
		var jump = cc.sequence(up,down,callback);
		// 执行动作
        this.player.runAction(jump);
		// X轴翻转，1表示正向，-1表示反向
		this.player.scaleX = 1;
		
		// 石头向左移动
		var goAction = cc.moveBy(0.2,cc.Vec2(60,-100));
        this.boxList.runAction(goAction);
    },
	jumpIsOver () {
		console.log('跳跃完成');
		// 跳跃完成，可以点击
		this.jump = true;
	},
	playerMoveRight () {
		console.log('向右跳跃');
		// 用0.1秒的时间，向上移动100px
		var up = cc.moveTo(0.1,cc.Vec2(this.player.x,this.player.y+100));
		// 用0.1秒的时间, 向下移动100px
		var down = cc.moveTo(0.1,cc.Vec2(this.player.x,this.player.y));
		// 执行回调
		var callback = cc.callFunc(this.jumpIsOver, this); 
		// 先向上移动，再向下移动
		var jump = cc.sequence(up,down,callback);
		// 执行动作
        this.player.runAction(jump);
		// X轴翻转，1表示正向，-1表示反向
		this.player.scaleX = -1;
		
		// 石头向右移动
		var goAction = cc.moveBy(0.2,cc.Vec2(-60,-100));
        this.boxList.runAction(goAction);
    },
	bgMove () {
		// 背景移动
		this.l1.y -= this.speed;
		this.l2.y -= this.speed;
		
		this.r1.y -= this.speed;
		this.r2.y -= this.speed;
 
		// 重置
		if (this.l1.y <= this.triggerLY) 
			this.l1.y = this.l2.y+this.l1.height;
		else if (this.l2.y <= this.triggerLY) 
			this.l2.y = this.l1.y+this.l1.height;
		
		if (this.r1.y <= this.triggerRY) 
			this.r1.y = this.r2.y+this.r1.height;
		else if (this.r2.y <= this.triggerRY) 
			this.r2.y = this.r1.y+this.r1.height;
	},
    start () {

    },

    // update (dt) {},
});
