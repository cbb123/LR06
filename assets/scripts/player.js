// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        isTouched: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
		this.node.parent.on(cc.Node.EventType.TOUCH_END,function(event){
			console.log('end');
			this.isTouched = false
			// 开启碰撞检测
			var manager = cc.director.getCollisionManager();
			manager.enabled = true;			
			this.scheduleOnce(function(){
                this.checkIsFailed();
				manager.enabled = false;		
            },0.15);
        },this);
	},
	checkIsFailed: function () {
        if (this.isTouched == false) {
            cc.log("fail");
            var goAction = cc.moveBy(0.2,cc.Vec2(0,-600));
            this.node.runAction(goAction);
			cc.director.loadScene("gameOver");
        }
    },

    start () {

    },
	onCollisionEnter: function(other, self) {
        console.log('enter'+other.tag);
		this.isTouched = true;
    },
    onCollisionStay: function (other, self) {
        console.log('stay');
    },
    onCollisionExit: function (other, self) {
        console.log('exit');
    },

    // update (dt) {},
});
